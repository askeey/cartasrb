load 'classes/Baralho.rb'

baralho_embaralhado = Baralho.new.tap do |b|
  b.mostrar
end

puts '=' * 15

baralho_ordenado = Baralho.new(false).tap do |b|
  b.mostrar
end