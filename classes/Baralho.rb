load 'classes/Carta.rb'

class Baralho
  attr_reader :cartas

  # =============================================
  #  construtor: Bool _embaralhar -> 0
  #  
  #  Populando o baralho (conjunto de cartas) com
  #  52 cartas.
  # =============================================
  def initialize(_embaralhar = true)
    @cartas = []
    montar

    _embaralhar ? embaralhar : return
  end

  # =============================================
  #  montar: 0 -> 0
  #  
  #  Populando o baralho (conjunto de cartas) com
  #  52 cartas.
  # =============================================
  def montar
    for naipe in ['♣', '♦', '♥', '♠'] do
      for numero in 1..13 do
        @cartas.append Carta.new(numero, naipe)
      end
    end
  end

  # =============================================
  #  embaralhar: 0 -> 0
  #  
  #  Embaralhando todas as 52 cartas do baralho.
  # =============================================
  def embaralhar
    (0..@cartas.length-1).each do |i|
      aleatorio = rand(0..i)
      @cartas[i], @cartas[aleatorio] = @cartas[aleatorio], @cartas[i]
    end
  end

  # =============================================
  #  mostrar: 0 -> 0
  #  
  #  Exibe todas as cartas do baralho.
  # =============================================
  def mostrar
    @cartas.each {|carta| puts carta.carta}
  end
end


b1 = Baralho.new(true)
b1.cartas.each_with_index {|carta, i| puts "#{i}. #{carta.carta}"}